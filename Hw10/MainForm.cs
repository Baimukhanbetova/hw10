﻿/*Разработать текстовый редактор, организовать открытие/сохране-ние текстовых файлов.
■ В панели инструментов расположить кнопки (Открыть, со-хранить, новый документ, копировать, вырезать, вставить, 
отменить, кнопка настройки редактора(цвет шрифта, цвет 
фона, шрифт)).
■ Меню должно дублировать панель инструментов (+ выде-лить все, + сохранить как);
■ В Заголовке окна находится полный путь к файлу;
■ Организовать контекстное меню для окна редактора (Ко-пировать, Вырезать, Вставить, Отменить)*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hw10
{
    public partial class MainForm : Form
    {
        string buffer;
        public MainForm()
        {
            InitializeComponent();
            showTextBox.ContextMenuStrip = contextMenuStrip;
        }

        private void OpenButton_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.Cancel)
                return;
            string filename = openFileDialog.FileName;
            string fileText = System.IO.File.ReadAllText(filename);
            showTextBox.Text = fileText;
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == DialogResult.Cancel)
                return;
            string filename = saveFileDialog.FileName;
            System.IO.File.WriteAllText(filename, showTextBox.Text);
        }

        private void OpenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenButton_Click(sender, e);
        }

        private void SaveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveButton_Click(sender, e);
        }

        private void ColorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (fontDialog.ShowDialog() == DialogResult.Cancel)
                return;
            colorToolStripMenuItem.ForeColor = fontDialog.Color;
        }

        private void BackColorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (colorDialog.ShowDialog() == DialogResult.Cancel)
                return;
            this.BackColor = colorDialog.Color;
        }

        private void FontToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (fontDialog.ShowDialog() == DialogResult.Cancel)
                return;
            fontToolStripMenuItem.Font = fontDialog.Font;
        }

        private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
           buffer = showTextBox.SelectedText;
        }

        private void PastedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showTextBox.Paste(buffer);
        }
    }
}
